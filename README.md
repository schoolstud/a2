# README #

This is how to install the applicaiton

### Steps to run the gradlew script? ###

* Run war, to package all

* Run your favourite servlet server. I used jetty, which is supported with my gradle also

![2.PNG](https://bitbucket.org/repo/Rkd7Ay/images/1000649589-2.PNG)


### How to use? ###

![11.PNG](https://bitbucket.org/repo/Rkd7Ay/images/1840241031-11.PNG)

* Go to login page

The starting credentials are

admin, 123456Qq1!
user, 123456Qq1!
!!!! do not remove the admin !!!!

* Logged in view

![22.PNG](https://bitbucket.org/repo/Rkd7Ay/images/1285847803-22.PNG)

* Edit own data

![33.PNG](https://bitbucket.org/repo/Rkd7Ay/images/2950884159-33.PNG)

* View users or remove

![44.PNG](https://bitbucket.org/repo/Rkd7Ay/images/3200176827-44.PNG)

* Create users

![55.PNG](https://bitbucket.org/repo/Rkd7Ay/images/2457436476-55.PNG)

* View and filter books or remove

![66.PNG](https://bitbucket.org/repo/Rkd7Ay/images/3374666021-66.PNG)

* Create books

![77.PNG](https://bitbucket.org/repo/Rkd7Ay/images/2977668471-77.PNG)

* The user view has limited actions

![88.PNG](https://bitbucket.org/repo/Rkd7Ay/images/2958741176-88.PNG)