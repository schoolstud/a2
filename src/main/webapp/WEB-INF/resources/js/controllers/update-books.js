app.controller('UpdateBooksCtrl', ['$scope', '$stateParams', function($scope, $stateParams) {
	$scope.id = $stateParams.id;
    $scope.errormsg = "Edit book details.";
    $scope.project = {
        title: "",
        author: "",
        genre: "",
        price: "",
        quantity: ""
    };
    $scope.init = function() {
        $.get(getBookUrl, { id: $stateParams.id, pageStart:0, pageCount:1}, function(data) {
            if (data == '') {} else {
                $scope.project = JSON.parse(data);
                if(role!='admin')
                $('.admOnly').remove();
            else
                $('.usrOnly').remove();
                $scope.$apply();
            }
        })
    }
    $scope.init();
    $scope.edit = function() {
        $.post(updateBookUrl, $('#projectForm').serialize(), function(data) {
            if (data == '') {
                $('.outerForm').hide('slow', function() {
                    $('.outerForm').show('slow');
                    $scope.errormsg = "Edit your user details.";
                    $scope.$apply();
                });
            } else {
                alert(data);
                $('.errmsg').hide('slow', function() {
                    $scope.errormsg = data;
                    $scope.$apply();
                });
            }
        });
    }
}]);
