app.controller('CreateBooksCtrl', ['$scope', function($scope) {
	$scope.role = role;
    $scope.errormsg = "Please fill the forms below.";
    $scope.project = {
        title: "",
        author: "",
        genre: "",
        price: "",
        quantity: ""
    };
    setTimeout(function(){
    	if(role=="user")
    	$('#adminOnly').remove();
	}, 0);
    $scope.register = function() {
        $.post(makeBookUrl, $('#projectForm').serialize(), function(data) {
            if (data == '') {
                $('#projectForm').hide('slow', function() {
                	$('#projectForm').show();
                    $scope.errormsg = "Book added successful.";
                    $scope.$apply();
                });
            } else {
                $('#errormsg').hide('slow', function() {
                    $('#errormsg').show('slow');
                    $scope.errormsg = data;
                    $scope.$apply();
                });
            }
        });
    }
}]);