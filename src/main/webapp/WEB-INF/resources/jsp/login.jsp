<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
    <head>
        <spring:url value="/resources/css/angular-material.min.css" var="angularMaterialCss" htmlEscape="true"/>
        <spring:url value="/resources/css/robotoFont.css" var="robotoFont" htmlEscape="true"/>
        <spring:url value="/resources/js/angular.min.js" var="angular" htmlEscape="true"/>
        <spring:url value="/resources/js/angular-animate.min.js" var="angularAnimate" htmlEscape="true"/>
        <spring:url value="/resources/js/angular-aria.min.js" var="angularAria" htmlEscape="true"/>
        <spring:url value="/resources/js/angular-material.min.js" var="angularMaterialJs" htmlEscape="true"/>
        <spring:url value="/resources/js/jquery-2.2.3.min.js" var="jQuery" htmlEscape="true"/>      
        <spring:url value="/resources/images/icon.png" var="icon" htmlEscape="true"/>
        <spring:url value="/resources/js/angular-messages.min.js" var="angularMesages" htmlEscape="true"/>
        <link rel="shortcut icon" type="image/x-icon" href="${icon}">
        <link rel="stylesheet" href="${robotoFont}">
        <link rel="stylesheet" href="${angularMaterialCss}">
        <script src="${jQuery}"></script>
      <script src="${angular}"></script>
      <script src="${angularAnimate}"></script>
      <script src="${angularAria}"></script>
      <script src="${angularMaterialJs}"></script>
      <script src="${angularMesages}"></script>
      <script>
        var app = angular.module('MainScreen', ['ngAnimate', 'ngAria', 'ngMaterial', 'ngMessages']);
        app.config(function($mdThemingProvider) {
        $mdThemingProvider.theme('default')
        .primaryPalette('indigo')
        .accentPalette('pink');
        });
        </script>
        <style>
        .page{
        height:100%;
        }
        </style>
        <title>
        Library App - Login Page
        </title>
    </head>
    <body ng-controller="AppCtrl" ng-app="MainScreen" ng-cloak>
        <div  layout="column" class="page">
            <md-toolbar class="md-primary">
            <div class="md-toolbar-tools">
                <a href = "/DaianLibrary/welcome"><h2 class="md-flex">Online Library</h2></a>
                <span flex></span>
                <c:if test="${pageContext.request.userPrincipal.name == null and empty error and empty register}">
                <h2 class="md-flex">Please login with your credentials below.</h2>
                </c:if>
                <c:if test="${pageContext.request.userPrincipal.name != null}">
                <h2 class="md-flex">You are already logged in. Redirecting to home page</h2>
                <script>
                window.location.replace("/DaianLibrary/welcome");
                </script>
                </c:if>
                <c:if test="${not empty error and empty register}">
                <h2 class="md-flex">Try logging again.</h2>
                </c:if>
                <c:if test="${not empty register}">
                    <h2 class="md-flex" id="errmsg">{{errormsg}}</h2>
                </c:if>
            </div>
            </md-toolbar>
            <div layout="column" flex ng-cloak>
                <md-content layout-padding>
                <!-- LOGIN FORM -->
                <c:if test="${pageContext.request.userPrincipal.name == null and empty register}">
                <script>
                app.controller('AppCtrl', ['$scope', function($scope) {
                }]);
                </script>
                <c:url var="loginUrl" value="/login"/>
                <form name="projectForm" autocomplete="on" layout="column" action="${loginUrl}" method="POST">
                    <input type="hidden"
                    name="${_csrf.parameterName}"
                    value="${_csrf.token}"/>
                    <!-- CLIENT NAME -->
                    <md-input-container class="md-block">
                    <label>Client Username</label>
                    <input required name="username" ng-model="project.username" id="username">
                    <div ng-messages="projectForm.username.$error">
                        <div ng-message="required">This is required.</div>
                        <div ng-message="pattern">This field has invalid characters.</div>
                    </div>
                    </md-input-container>
                    <!-- CLIENT PASS -->
                    <md-input-container class="md-block">
                    <label>Client Password</label>
                    <input id="password" required type="password" name="password" ng-model="project.password"
                    minlength="8" maxlength="25" ng-pattern="/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/"/>
                    <div ng-messages="projectForm.password.$error" role="alert">
                        <div ng-message-exp="['required', 'minlength', 'maxlength', 'pattern']">
                            Your password must have minimum 8 characters, at least 1 Uppercase Alphabet, 1 Lowercase Alphabet and 1 Number.
                        </div>
                    </div>
                    </md-input-container>
                    <md-button class="md-raised md-accent" flex type="submit">
                    Login
                    </md-button>
                </form>
                </md-content>
                </c:if>
            </div>
            <md-toolbar class="md-primary">
            <div class="md-toolbar-tools">
                <a href = "/DaianLibrary/welcome"><h2 class="md-flex">Online Library</h2></a>
                <span flex></span>
            </div>
            </md-toolbar>
        </div>
    </body>
</html>