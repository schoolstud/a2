package com.daian.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daian.db.*;
import com.daian.db.User;
import com.daian.db.UserRole;
import com.daian.db.service.DatabaseService;
import com.daian.db.service.ResultView;
import com.daian.misc.CreateCSV;
import com.daian.misc.CreatePDF;
import com.daian.misc.CreateReport;
import com.daian.security.CustomUserDetail;

@Service
public class HomeService {

	static Logger log = Logger.getLogger(CustomUserDetail.class.getName()); 
	@Autowired
	public DatabaseService dbService;
	public  Map<Class<?>, CreateReport> creator = new HashMap<Class<?>, CreateReport>();

	public HomeService(){
		creator.put(CreateCSV.class, new CreateCSV());
		creator.put(CreatePDF.class, new CreatePDF());
	}
	
	public void editUser(String pass, String name, String sur, String email, String address, String city, String state)
			throws Exception { 

		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		User user = ((CustomUserDetail) userDetails).getUser();

		// do this to check for errors
		User changes = new User(user.username, pass, name, sur, user.numeric_personal_number, address, city,
				state, email, user.enabled);
		changes.nullCheck();

			user.name = name;
			user.address = address;
			user.city = city;
			user.email=email;
			user.password=pass;
			user.state=state;
			user.surname=sur;
	}

	public List<User> findBy(int start, int count, String user, String name, String surname, String city, String state,
			String email, String cnp, Class<?> cls) throws Exception {
		ResultView res = new ResultView(dbService.factory, User.class);
		boolean ok = false;
		if (name != null) {
			res.andByString(name, "name");
			ok = true;
		}
		if (user != null) {
			res.andByString(user, "username");
			ok = true;
		}
		if (surname != null) {
			res.andByString(surname, "surname");
			ok = true;
		}
		if (city != null) {
			res.andByString(city, "city");
			ok = true;
		}
		if (state != null) {
			res.andByString(state, "state");
			ok = true;
		}
		if (email != null) {
			res.andByString(email, "email");
			ok = true;
		}
		if (cnp != null) {
			res.andByString(cnp, "numeric_personal_number");
			ok = true;
		}
		if(ok == false){
			log.debug("got in");
			res = dbService.factory.findByString("", User.class, "username");
			log.debug(res.getResult().toString());
		}
		return res.getResult();
	}

	public DatabaseService getDbService() {
		return dbService;
	}

	public void makeBook(String title, String author, String genre, int price, int quantity)
			throws Exception {
		Book userObj = new Book(title, author, genre, quantity ,price);
		userObj.nullCheck();
		
		dbService.factory.add(userObj, Book.class);
	}

	public void makeUser(String user, String pass, String name, String sur, String cnp, String address, String city,
			String state, String email) throws Exception {
		User userObj = new User(user, pass, name, sur, cnp, address, city, state, email, 1);
		userObj.nullCheck();
		// these throw if they dont find
		if(!dbService.factory.findByString(user, User.class, "username").getResult().isEmpty()){
			throw new Exception("Username is already in use.");				
		}
		if(!dbService.factory.findByString(cnp, User.class, "numeric_personal_number").getResult().isEmpty()){
			throw new Exception("Numeric number is already in use.");				
		}
		dbService.factory.add(userObj, User.class);
		dbService.factory.add(new UserRole(user, "ROLE_USER"), UserRole.class);
	}

	public void removeData(int id, Class<?> cls) throws Exception {
		// also delete roles if user
		Object usr = dbService.factory.get(id, cls);
		if (cls.equals(User.class)) {
			List<UserRole> roles = dbService.factory.findByString(((User)usr).username, UserRole.class, "role").getResult();
			for(UserRole role : roles){
				dbService.factory.remove(role.id, UserRole.class);
			}
		}
		dbService.factory.remove(id, cls);
	}

	public void setDbService(DatabaseService dbService) {
		this.dbService = dbService;
	}

	public void updateBook(int id, String title, String author, String genre, int quantity, int price)
			throws Exception {
		Book foundObj = dbService.factory.get(id, Book.class);
		if(foundObj == null){
			throw new Exception("Book not found");
		}
		Book userObj = new Book(title, author, genre, quantity, price);
		try{
		userObj.nullCheck();
		}catch(Exception e){
			
		}
		dbService.factory.begin(foundObj.id, Book.class);
		if(title!=null)
		foundObj.title=title;
		if(author!=null)
			foundObj.author=author;
		if(genre!=null)
			foundObj.genre=genre;
		if(quantity>=0)
			foundObj.quantity=quantity;
		if(price>=0)
			foundObj.price=price;
			dbService.factory.end(foundObj.id, Book.class);
	}

	public void updateUser(String username, String name, String sur, String address, String city, String state,
			String email) throws Exception {
		User foundObj = dbService.factory.findByString(username, User.class, "username").getResult().iterator().next();
		User userObj = new User(foundObj.username, foundObj.password, name, sur,
				foundObj.numeric_personal_number, address, city, state, email, 1);
		userObj.nullCheck();
		dbService.factory.begin(foundObj.id, User.class);
		foundObj.name=name;
		foundObj.address=address;
		foundObj.city=city;
		foundObj.email=email;
		foundObj.state=state;
		foundObj.surname=sur;
		dbService.factory.end(foundObj.id, User.class);
	}

	public String findBook(int id) throws Exception {
		Book foundObj = dbService.factory.get(id, Book.class);
		return foundObj.toString();
	}
	
	public String findBook(int st, int cnt, String title, String author, String genre) throws Exception {
		ResultView<Book> res = new ResultView<Book>(dbService.factory, Book.class);
		boolean found = false;
		if (title != null) {
			res.andByString(title, "title");
			found = true;
		}
		if (author != null) {
			res.andByString(author, "author");
			found = true;
		}
		if (genre != null) {
			res.andByString(genre, "genre");
			found = true;
		}
		if(found == false){
			res = dbService.factory.findByString("", Book.class, "title");
		}
		int n = res.getResult().size();
		if(n==0)
			n=1;
		if(st<0)
			st =0;
		int toI = st + cnt;
		if(toI<0)
			toI = 0;
		List<Book> books = res.getResult();
		books.subList(st>n?n-1 :st,  toI>n ? n-1 : toI);
		return books.toString();
	}
}