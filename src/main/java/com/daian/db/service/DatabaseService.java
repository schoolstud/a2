package com.daian.db.service;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.daian.db.Book;
import com.daian.db.User;
import com.daian.db.UserRole;
import com.daian.security.UserDetailsAdapter;

@Repository
public class DatabaseService {
	public DbHolder factory;
	static Logger log = Logger.getLogger(UserDetailsAdapter.class.getName());
	private boolean XML_LOAD = true;
	
	public DatabaseService() {
		if(XML_LOAD){
			try {
				factory = DbHolder.load();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace(); 
				XML_LOAD = false;
			}
		}
		if(!XML_LOAD){
			factory = new DbHolder();
			try {
				factory.add(new User("admin", "123456Qq1!", "Daian", "Dragos", "1234567890123", "home", "city", "state", "email", 1), User.class);
				factory.add(new UserRole("admin", "ROLE_ADMIN"), UserRole.class);
				factory.add(new UserRole("admin", "ROLE_USER"), UserRole.class);
				factory.add(new User("user", "123456Qq1!", "Daian", "Dragos", "1234567890123", "home", "city", "state", "email", 1), User.class);
				factory.add(new UserRole("user", "ROLE_USER"), UserRole.class);
				factory.add(new Book("Opengl Red Book", "author", "adventure", 10, 99), Book.class);
				factory.add(new Book("Opengl Red Book 2", "author", "adventure", 20, 10), Book.class);
				factory.add(new Book("Opengl Red Book 3", "other", "info", 10, 99), Book.class);
				factory.add(new Book("Opengl Red Book 4", "other", "info", 20, 10), Book.class);
				factory.add(new Book("Opengl Red Book 5", "other", "horror", 20, 10), Book.class);
				factory.add(new Book("Opengl Red Book 6", "other", "horror", 20, 10), Book.class);
				factory.add(new Book("Opengl Red Book 7", "other", "horror", 20, 10), Book.class);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
