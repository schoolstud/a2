package com.daian.db.service;

import java.io.*;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlRootElement;

import com.daian.db.Book;
import com.daian.db.User;
import com.daian.db.UserRole;
import com.daian.misc.LetterNode;
import com.daian.misc.Trie;
import com.thoughtworks.xstream.XStream;

@XmlRootElement
public class DbHolder implements java.io.Serializable{
	// also keep them in a Map, for O(1) retrieval by id.
	private Map<Class<?>, Map<Integer, Object>> maps = new HashMap<Class<?>, Map<Integer, Object>>();
	// these are for strings, support for fuzzy search. O(log(n)) for search.
	private Map<Class<?>, Map<Field, Trie>> strings = new HashMap<Class<?>, Map<Field, Trie>>();
	// these are for ints, support for compares. Also O(log(n)) for search.
	private Map<Class<?>, Map<Field, TreeMap<Integer, Set<Integer>>>> ints = new HashMap<Class<?>, Map<Field, TreeMap<Integer, Set<Integer>>>>();
	public enum IntegerComparison{
		Equal, Greater, Less
	};
	// this map is for last id information.
	private Map<Class<?>, Integer> lastId = new HashMap<Class<?>, Integer>();
	
	private void addToTree(int id, Object obj, Class<?> cls) throws Exception{
		Field[] fields = cls.getFields();
		for(Field field : fields){
			Object fieldValue = field.get(obj);
			if(fieldValue.getClass().equals(Integer.class)){
				if(ints.get(cls) == null){
					ints.put(cls, new HashMap<Field, TreeMap<Integer, Set<Integer>>>());
				}
				if(ints.get(cls).get(field) == null){
					ints.get(cls).put(field, new TreeMap<Integer, Set<Integer>>());
				}
				if(ints.get(cls).get(field).get((Integer)fieldValue)==null){
					ints.get(cls).get(field).put((Integer)fieldValue, new TreeSet<Integer>());
				}
				ints.get(cls).get(field).get((Integer)fieldValue).add(id);
			}else if(fieldValue.getClass().equals(String.class)){
				if(strings.get(cls) == null){
					strings.put(cls, new HashMap<Field, Trie>());
				}
				if(strings.get(cls).get(field) == null){
					strings.get(cls).put(field, new Trie());
				}
				strings.get(cls).get(field).addWord((String)fieldValue, id);
			}else{
				throw new Exception("Data type not recognized " + fieldValue.getClass().getName());
			}
		}
	}
	
	public void save() throws Exception{
		XStream xstream = new XStream();
		String xml = xstream.toXML(this);
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(
	              new FileOutputStream("ser.xml"), "utf-8"))) {
		   writer.write(xml);
		}
	}
	
	public static DbHolder load() throws Exception{
		XStream xstream = new XStream();
		File file = new File("ser.txt");
		FileInputStream fis = new FileInputStream(file);
		byte[] data = new byte[(int) file.length()];
		fis.read(data);
		fis.close();

		String str = new String(data, "UTF-8");
		return (DbHolder) xstream.fromXML(str);
	}
	
	private void removeFromTree(Integer id, Object obj, Class<?> cls) throws Exception{		
		Field[] fields = cls.getFields();
		for(Field field : fields){
			Object fieldValue = field.get(obj);
			if(fieldValue.getClass().equals(Integer.class)){
				ints.get(cls).get(field).remove((Integer)fieldValue);
			}else if(fieldValue.getClass().equals(String.class)){
				strings.get(cls).get(field).removeWord((String)fieldValue, id);
			}else{
				throw new Exception("Data type not recognized " + fieldValue.getClass().getName());
			}
		}
		save();
	}
	
	public int add(Object obj, Class<?> cls) throws Exception{
		Integer id = lastId.get(cls);
		if(id == null){
			id = 0;
		}
		id++;
		cls.getField("id").set(obj, id);
		lastId.put(cls, id);
		if(maps.get(cls) == null){
			maps.put(cls, new HashMap<Integer, Object>());
		}
		maps.get(cls).put(id, obj);
		addToTree(id, obj, cls);
		return id;
	}
	
	public <T> T get(Integer id, Class<T> cls){
		Map<Integer, Object> map = maps.get(cls);
		if(map == null){
			return null;
		}
		return (T) map.get(id);
	}
	
	public void remove(Integer id, Class<?> cls) throws Exception{
		Map<Integer, Object> map = maps.get(cls);
		if(map == null){
			return;
		}
		removeFromTree(id, get(id, cls), cls);
		map.remove(id);
	}
	
	public void begin(Integer id, Class<?> cls){
		try {
			removeFromTree(id, get(id, cls), cls);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void end(Integer id, Class<?> cls){
		try {
			addToTree(id, get(id, cls), cls);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public <T> ResultView<T> findByInteger(Integer rule, Class<T> cls, String fieldName, IntegerComparison type) throws Exception{
		Set<T> ret = new TreeSet<T>(new Comparator<T>() {
			@Override
		    public int compare(T a, T b) {				
		        try {
					return Integer.compare(cls.getField("id").getInt(a), cls.getField("id").getInt(b));
				} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException
						| SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		        return 0;
		    }
		});
		Field field = cls.getField(fieldName);
		if(!field.getType().equals(String.class)){
			Set<Integer> id;
			int index = 0;
			SortedMap<Integer, Set<Integer>> ids = null;
			TreeMap<Integer, Set<Integer>> treemap = ints.get(cls).get(field);
			switch(type){
			case Equal:
				id = treemap.get(rule);
				for(Integer idd : id){
					ret.add((T)maps.get(cls).get(idd));
				}
				break;
			case Less:
				ids = treemap.headMap(rule);
				for(Map.Entry<Integer, Set<Integer>> entry : ids.entrySet()){
					Set<Integer> value = entry.getValue();
					for(Integer idd : value){
						ret.add((T)maps.get(cls).get(idd));
					}
				}
				break;
			case Greater:
				ids = treemap.tailMap(rule);
				for(Map.Entry<Integer, Set<Integer>> entry : ids.entrySet()){
					Set<Integer> value = entry.getValue();
					for(Integer idd : value){
						ret.add((T)maps.get(cls).get(idd));
					}
				}
				break;
			}
		}
		return new ResultView<T>(this, cls, ret);
	}
	
	public <T> ResultView<T> findByString(String rule, Class<T> cls, String fieldName) throws Exception{
		Set<T> ret = new TreeSet<T>(new Comparator<T>() {
			@Override
		    public int compare(T a, T b) {				
		        try {
					return Integer.compare(cls.getField("id").getInt(a), cls.getField("id").getInt(b));
				} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException
						| SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		        return 0;
		    }
		});
		Field field = cls.getField(fieldName);
		if(field.getType().equals(String.class)){
			Trie trie = strings.get(cls).get(field);
			List<Integer> ids = trie.findAll(rule);
			for(Integer id : ids){
				T bb = (T) maps.get(cls).get(id);
				ret.add(bb);
			}
		}
		return new ResultView<T>(this, cls, ret);
	}
}
