package com.daian.db.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.daian.db.service.DbHolder.IntegerComparison;

public class ResultView<T> implements java.io.Serializable{
	private final DbHolder holder;
	private Set<T> result ;
	private boolean empty = true;
	private Class<T> cls;
	
	public ResultView(DbHolder dbHolder, Class<T> cls, Set<T> ret) {
		this.holder = dbHolder;
		this.empty = false;
		this.cls = cls;
		this.result = ret;
	}
	
	public ResultView(DbHolder dbHolder, Class<T> cls) {
		this.holder = dbHolder;
		this.empty = true;
		this.cls = cls;
		result = new TreeSet<T>(new Comparator<T>() {
			@Override
		    public int compare(T a, T b) {				
		        try {
					return Integer.compare(cls.getField("id").getInt(a), cls.getField("id").getInt(b));
				} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException
						| SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		        return 0;
		    }
		});
	}

	public ResultView<T> orByInteger(Integer rule, String fieldName, IntegerComparison type) throws Exception{
		ResultView<T> secondResult = holder.findByInteger(rule, cls, fieldName, type);
		result.addAll(secondResult.getResult());
		return this;
	}	

	public ResultView<T> orByString(String rule, String fieldName) throws Exception{
		ResultView<T> secondResult = holder.findByString(rule, cls, fieldName);
		result.addAll(secondResult.getResult());
		return this;
	}

	public ResultView<T> andByInteger(Integer rule, String fieldName, IntegerComparison type) throws Exception{
		if(empty){
			empty = false;
			return orByInteger(rule, fieldName, type);
		}
		ResultView<T> secondResult = holder.findByInteger(rule, cls, fieldName, type);
		result.retainAll(secondResult.getResult());
		return this;
	}	

	public ResultView<T> andByString(String rule, String fieldName) throws Exception{
		if(empty){
			empty = false;
			return orByString(rule, fieldName);
		}
		ResultView<T> secondResult = holder.findByString(rule, cls, fieldName);
		result.retainAll(secondResult.getResult());
		return this;
	}
	
	public List<T> getResult(){
		return new ArrayList<T>(result);
	}
}
