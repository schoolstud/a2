package com.daian.db;

import java.util.HashSet;
import java.util.Set;

public class Book implements java.io.Serializable{
	public String title;
	public String author;
	public String genre;
	public int id;
	public int quantity;
	public int price;
	public Book(){
		
	}
	public Book(String title, String author, String genre, int id, int quantity, int price) {
		super();
		this.title = title;
		this.author = author;
		this.genre = genre;
		this.id = id;
		this.quantity = quantity;
		this.price = price;
	}
	public Book(String title, String author, String genre, int quantity, int price) {
		super();
		this.title = title;
		this.author = author;
		this.genre = genre;
		this.quantity = quantity;
		this.price = price;
	}
	@Override
	public String toString() {
		return "{\"title\":\"" + title + "\",\"author\":\"" + author + "\",\"genre\":\"" + genre + "\",\"id\":\"" + id
				+ "\",\"quantity\":\"" + quantity + "\",\"price\":\"" + price + "\"}";
	}

	public void nullCheck() throws Exception {
		if (title == null || title.equals(""))
			throw new Exception("Title is empty");
		if (title == null || author.equals(""))
			throw new Exception("Author is empty");
		if (title == null || genre.equals(""))
			throw new Exception("Genre is empty");
		if (quantity < 0)
			throw new Exception("Quantity is negative");
		if (price < 0)
			throw new Exception("Price is negative");
	}

}