package com.daian.db;

public class UserRole implements java.io.Serializable{
	public int id;
	public String role;
	public String username;

	public UserRole() {

	}

	public UserRole(int id, String username, String role) {
		super();
		this.id = id;
		this.username = username;
		this.role = role;
	}

	public UserRole(String username, String role) {
		super();
		this.username = username;
		this.role = role;
	}
	public void nullCheck() throws Exception {
		if (username == null)
			throw new Exception("Username is null.");
		if (role == null)
			throw new Exception("Role is null.");
	}

	@Override
	public String toString() {
		return "{\"id\":\"" + id + "\",\"role\":\"" + role + "\",\"username\":\"" + username + "\"}";
	}

}