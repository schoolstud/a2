package com.daian.db;

import java.util.HashSet;
import java.util.Set;

public class User implements java.io.Serializable{
	public String address;
	public String city;
	public String email;
	public int enabled;
	public int id;
	public String name;
	public String numeric_personal_number;
	public String password;
	public String state;
	public String surname;
	public String username;

	public User() {
	}

	public User(String username, String password, String name, String surname, String numeric_personal_number,
			String address, String city, String state, String email, int enabled) {
		super();
		this.username = username;
		this.password = password;
		this.name = name;
		this.surname = surname;
		this.numeric_personal_number = numeric_personal_number;
		this.address = address;
		this.enabled = enabled;
		this.city = city;
		this.state = state;
		this.email = email;
	}

	public void nullCheck() throws Exception {
		if (username == null)
			throw new Exception("Username is empty");
		if (password == null)
			throw new Exception("Password is empty");
		if (name.equals(""))
			throw new Exception("Name is empty");
		if (surname == null)
			throw new Exception("Surname is empty");
		if (city == null)
			throw new Exception("City is empty");
		if (state == null)
			throw new Exception("State is empty");
		if (numeric_personal_number == null)
			throw new Exception("Numeric Personal Code is empty");
		if (address == null)
			throw new Exception("Address is empty");
		if (email == null)
			throw new Exception("Email is empty");
	}

	@Override
	public String toString() {
		return "{\"address\":\"" + address + "\",\"city\":\"" + city + "\",\"email\":\"" + email + "\",\"enabled\":\""
				+ enabled + "\",\"id\":\"" + id + "\",\"name\":\"" + name + "\",\"numeric_personal_number\":\""
				+ numeric_personal_number + "\",\"password\":\"" + password + "\",\"state\":\"" + state
				+ "\",\"surname\":\"" + surname + "\",\"username\":\"" + username + "\"}";
	}

}