package com.daian.misc;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LetterNode implements java.io.Serializable
{
   private Map<Character, LetterNode> children;
   protected List<Integer> ids = null;
   protected char Letter;

   public LetterNode(){
      children = new HashMap<Character, LetterNode>();
   }

   protected LetterNode(char character){
      this();
      this.Letter = character;
   }
   
   protected void addWord(String word, int id){
	  if(word.isEmpty()){
    	  if(ids == null)
    		  ids = new ArrayList<Integer>();
    	  ids.add(id);
    	  return;
		  
	  }
	  char letter = word.charAt(0);
      if (children.get(letter) == null){
    	  children.put(letter, new LetterNode(word.charAt(0)));
      }
      
      children.get(letter).addWord(word.substring(1), id);
   }

   protected boolean removeWord(String word, int id) {
	   boolean recursiveReturn =  true;
	      if (word.length() > 0){
		      char letter = word.charAt(0);
		      recursiveReturn = children.get(letter).removeWord(word.substring(1), id);
	    	  if(recursiveReturn){
	    		  children.remove(letter);
	    	  }
	      }	      
	      if(children.size() == 0){
	    	  if(ids != null)
	    		  ids.remove(new Integer(id));
	      }
	      return recursiveReturn && (ids == null || ids.isEmpty() )&& children.isEmpty();
	}

   private List<Integer> getAll(){
	   List<Integer> values = new ArrayList<Integer>();
	   for(LetterNode node : children.values()){
		   values.addAll(node.getAll());
	   }
	   if(ids!=null){
		   values.addAll(ids);
	   }
	   return values;
   }
   
   public List<Integer> getWords(String word) {
	    if(word.isEmpty()){
	    	return getAll();
	    }
	    
		char letter = word.charAt(0);
	    
	    // return no elements
	    if (children.get(letter) != null){
	    	return children.get(letter).getWords(word.substring(1));
	    }
	    
	    // if some other error occurs, return also empty array, not null
    	return new ArrayList<Integer>();
	}
}