package com.daian.misc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import com.daian.db.Book;

public class CreateCSV extends CreateReport{

	@Override
	public void create(List<Book> books, String filename) {		
		// before we open the file check to see if it already exists
		boolean alreadyExists = new File(filename).exists();
			
		try {
			PrintWriter writer = new PrintWriter(filename, "UTF-8");
			writer.println("Title,Author,Genre,Price");
			for(Book book : books){
				writer.println(book.title+","+book.author +","+book.genre+","+book.price);				
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
