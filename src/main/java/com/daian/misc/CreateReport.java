package com.daian.misc;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.daian.db.Book;
import com.daian.db.service.DbHolder;
import com.daian.db.service.DbHolder.IntegerComparison;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public abstract class CreateReport {
	public void makeFrom(DbHolder database, String filename) throws Exception{
		List<Book> books = database.findByInteger(1, Book.class, "quantity", IntegerComparison.Less).getResult();
		create(books, filename);
	}
	
	public abstract void create(List<Book> books, String filename);
}
