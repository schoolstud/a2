package com.daian.misc;

import java.util.List;

public class Trie implements java.io.Serializable
{
   private LetterNode root;
   
   public Trie(){
      root = new LetterNode();
   }
   
   public void addWord(String word, int id){
      root.addWord(word, id);
   }
   
   public void removeWord(String word, int id){
	   root.removeWord(word, id);
   }
   
   public List<Integer> findAll(String prefix){
	   return root.getWords(prefix);
   }
}