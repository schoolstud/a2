package com.daian.web;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.daian.db.*;
import com.daian.misc.CreateCSV;
import com.daian.misc.CreatePDF;
import com.daian.security.CustomUserDetail;
import com.daian.service.HomeService;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
 
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
@Controller
public class HomeController {

	static Logger log = Logger.getLogger(HomeController.class.getName());
	private final HomeService homeService;
	Random rand = new Random();
	
	@Autowired
	public HomeController(HomeService helloWorldService) {
		this.homeService = helloWorldService;
	}

	// for 403 access denied page
	@RequestMapping(value = "/403")
	public ModelAndView accesssDenied() {

		ModelAndView model = new ModelAndView();

		// check if user is login
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken) && auth != null) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			model.addObject("username", userDetail.getUsername());
		}

		model.setViewName("403");
		return model;

	}

	@RequestMapping(value = { "/", "/welcome**" }, method = RequestMethod.GET)
	public ModelAndView defaultPage() {

		ModelAndView model = new ModelAndView();
		
		log.debug(SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString());

		model.setViewName("index");
		return model;

	}
	
	@RequestMapping(value = "/getBook", method = RequestMethod.GET)
	@ResponseBody 
	public String getBook(@RequestParam(value = "id", required = false) String id,
			@RequestParam(value = "pageStart", required = true) String pageStart,
			@RequestParam(value = "pageCount", required = true) String pageCount,
			@RequestParam(value = "title", required = false) String title,
			@RequestParam(value = "author", required = false) String author,
			@RequestParam(value = "genre", required = false) String genre) {
		String ret = "";

		try {
			if(id != null)
			ret = homeService.findBook(Integer.parseInt(id));
			else{
				ret = homeService.findBook(Integer.parseInt(pageStart), Integer.parseInt(pageCount), title, author, genre);
			}
		} catch (Exception e) {
			ret = e.getMessage();
		}
		return ret;
	}

	@RequestMapping(value = "/getCurrentData", method = RequestMethod.GET)
	@ResponseBody
	public String getDataUser() {
		String ret = "";

		try {
			UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			User user = ((CustomUserDetail) userDetails).getUser();
			List<User> users = homeService.findBy(0, 1, user.username, user.name, user.surname,
					user.city, user.state, user.email, user.numeric_personal_number, User.class);
			ret = users.toString();
		} catch (Exception e) {
			ret = e.getMessage();
		}
		return ret;
	}

	@RequestMapping(value = "/getDataUser", method = RequestMethod.GET)
	@ResponseBody
	public String getDataUser(@RequestParam(value = "username", required = false) String username,
			@RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "surname", required = false) String surname,
			@RequestParam(value = "city", required = false) String city,
			@RequestParam(value = "state", required = false) String state,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "cnp", required = false) String cnp,
			@RequestParam(value = "pageStart", required = true) String pageStart,
			@RequestParam(value = "pageCount", required = true) String pageCount) {
		String ret = "";

		try {
			List<User> user = homeService.findBy(Integer.parseInt(pageStart), Integer.parseInt(pageCount), username,
					name, surname, city, state, email, cnp, User.class);
			int toIndex = Integer.valueOf(pageStart) + Integer.valueOf(pageCount), fromIndex = Integer.valueOf(pageStart);
			user.subList(fromIndex>user.size()?user.size()-1 :fromIndex,  toIndex>user.size() ? user.size()-1 : toIndex);
			ret = user.toString();
		} catch (Exception e) {
			ret = e.getMessage();
		}
		return ret;
	}

	@RequestMapping(value = "/login")
	public ModelAndView login(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout,
			@RequestParam(value = "register", required = false) String register) {

		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "Invalid username and password!");
		}

		if (register != null) {
			model.addObject("register", "Register an account!");
		}

		model.setViewName("login");

		return model;

	}

	@RequestMapping(value = "/editUser", method = RequestMethod.POST)
	@ResponseBody
	public String register(@RequestParam(value = "password", required = true) String pass,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "surname", required = true) String sur,
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "address", required = true) String address,
			@RequestParam(value = "city", required = true) String city,
			@RequestParam(value = "state", required = true) String state) {

		String ret = "";

		try {
			homeService.editUser(pass, name, sur, email, address, city, state);
		} catch (Exception e) {
			ret = e.getMessage();
		}
		return ret;
	}
	
	@RequestMapping(value = "/makeBook", method = RequestMethod.POST)
	@ResponseBody
	public String registerClient(@RequestParam(value = "title", required = true) String title,
			@RequestParam(value = "author", required = true) String author,
			@RequestParam(value = "genre", required = true) String genre,
			@RequestParam(value = "price", required = true) String price,
			@RequestParam(value = "quantity", required = true) String quantity) {
		String ret = "";
		try {
			homeService.makeBook(title, author, genre, Integer.parseInt(price), Integer.parseInt(quantity));
		} catch (Exception e) {
			ret = e.getMessage();
		}
		return ret;
	}

	@RequestMapping(value = "/registerUser", method = RequestMethod.POST)
	@ResponseBody
	public String registerUser(@RequestParam(value = "user", required = true) String user,
			@RequestParam(value = "password", required = true) String pass,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "surname", required = true) String sur,
			@RequestParam(value = "cnp", required = true) String cnp,
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "address", required = true) String address,
			@RequestParam(value = "city", required = true) String city,
			@RequestParam(value = "state", required = true) String state) {
		String ret = "";
		try {
			homeService.makeUser(user, pass, name, sur, cnp, address, city, state, email);
		} catch (Exception e) {
			ret = e.getMessage();
		}
		return ret;
	}

	@RequestMapping(value = "/removeBook", method = RequestMethod.POST)
	@ResponseBody
	public String removeBook(@RequestParam(value = "id", required = true) String id) {
		String ret = "";
		try {
			homeService.removeData(Integer.parseInt(id), Book.class);
		} catch (Exception e) {
			ret = e.getMessage();
		}
		return ret;
	}

	@RequestMapping(value = "/removeUser", method = RequestMethod.POST)
	@ResponseBody
	public String removeUser(@RequestParam(value = "id", required = true) String id) {
		String ret = "";
		try {
			homeService.removeData(Integer.parseInt(id), User.class);
		} catch (Exception e) {
			ret = e.getMessage();
		}
		return ret;
	}

	@RequestMapping(value = "/updateBook", method = RequestMethod.POST)
	@ResponseBody
	public String updateClient(@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "title", required = false) String title,
			@RequestParam(value = "author", required = false) String author,
			@RequestParam(value = "genre", required = false) String genre,
			@RequestParam(value = "quantity", required = true) String quantity,
			@RequestParam(value = "price", required = false) String price) {

		String ret = "";

		try {
			int idP = -1;if(id != null)idP = Integer.parseInt(id);
			int quantityP = -1;if(quantity != null)quantityP = Integer.parseInt(quantity);
			int priceP = -1;if(price != null)priceP = Integer.parseInt(price);
			homeService.updateBook(idP, title, author, genre, quantityP, priceP);

		} catch (Exception e) {
			ret = e.getMessage();
		}
		return ret;
	}

	@RequestMapping(value = "/updateUser", method = RequestMethod.POST)
	@ResponseBody
	public String updateUser(@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "surname", required = true) String sur,
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "address", required = true) String address,
			@RequestParam(value = "city", required = true) String city,
			@RequestParam(value = "state", required = true) String state) {

		String ret = "";

		try {
			homeService.updateUser(username, name, sur, address, city, state, email);

		} catch (Exception e) {
			ret = e.getMessage();
		}
		return ret;
	}
	
	@RequestMapping(value = "/downloadCSV")
	public void downloadCSV(HttpServletRequest request, HttpServletResponse response) throws IOException {
 
		final ServletContext servletContext = request.getSession().getServletContext();
	    final File tempDirectory = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
	    final String temperotyFilePath = tempDirectory.getAbsolutePath();
 
	    String fileName = "Report.csv";
	    response.setContentType("application/csv");
	    response.setHeader("Content-disposition", "attachment; filename="+ fileName);
 
	    try {
	        homeService.creator.get(CreateCSV.class).makeFrom(homeService.dbService.factory, temperotyFilePath+"\\"+fileName);
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        baos = convertToByteArrayOutputStream(temperotyFilePath+"\\"+fileName);
	        OutputStream os = response.getOutputStream();
	        baos.writeTo(os);
	        os.flush();
	    } catch (Exception e1) {
	        e1.printStackTrace();
	    } 
	}
	
	@RequestMapping(value = "/downloadPDF")
	public void downloadPDF(HttpServletRequest request, HttpServletResponse response) throws IOException {
 
		final ServletContext servletContext = request.getSession().getServletContext();
	    final File tempDirectory = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
	    final String temperotyFilePath = tempDirectory.getAbsolutePath();
 
	    String fileName = "Report.pdf";
	    response.setContentType("application/pdf");
	    response.setHeader("Content-disposition", "attachment; filename="+ fileName);
 
	    try {
	        homeService.creator.get(CreatePDF.class).makeFrom(homeService.dbService.factory, temperotyFilePath+"\\"+fileName);
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        baos = convertToByteArrayOutputStream(temperotyFilePath+"\\"+fileName);
	        OutputStream os = response.getOutputStream();
	        baos.writeTo(os);
	        os.flush();
	    } catch (Exception e1) {
	        e1.printStackTrace();
	    } 
	}
	
	private ByteArrayOutputStream convertToByteArrayOutputStream(String fileName) {
		 
		InputStream inputStream = null;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
 
			inputStream = new FileInputStream(fileName);
			byte[] buffer = new byte[1024];
			baos = new ByteArrayOutputStream();
 
			int bytesRead;
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				baos.write(buffer, 0, bytesRead);
			}
 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return baos;
	}
}