package com.daian.security;

import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.daian.db.User;
import com.daian.db.UserRole;
import com.daian.db.service.DatabaseService;

@Service
public class UserDetailsAdapter implements UserDetailsService {
	@Autowired
	private DatabaseService dbService;
	
	static Logger log = Logger.getLogger(UserDetailsAdapter.class.getName());

	public DatabaseService getDbService() {
		return dbService;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		log.debug("Entered the load by user " + username);
		User user = null;
		try {
			user = dbService.factory.findByString(username, User.class, "username").getResult().iterator().next();
		} catch (Exception e) {
			// This should never ever happen
			// if it does, send an null filled user detail as requested by
			// spring
			//e.printStackTrace();
			throw new UsernameNotFoundException(username);
		}
		return new CustomUserDetail(user, dbService);
	}

	public void setDbService(DatabaseService dbService) {
		this.dbService = dbService;
	}

}