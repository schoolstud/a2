import org.junit.Test;

import com.daian.db.Book;
import com.daian.db.User;
import com.daian.db.service.DatabaseService;
import com.daian.misc.CreateCSV;
import com.daian.service.HomeService;

import org.junit.Assert;

public class DbTester {
	public static void main(String[] args){
		new DbTester().TestDb();
	}
	
	@Test
	public void TestDb(){

		try {
			DatabaseService dbService = new DatabaseService();
			HomeService srv = new HomeService();
			srv.dbService = dbService;
			User user = dbService.factory.get(2, User.class);
			Assert.assertEquals(user.username, "user");
			User admin = dbService.factory.get(1, User.class);
			Assert.assertEquals(admin.username, "admin");
			Assert.assertFalse(dbService.factory.findByString("admin", User.class, "username").getResult().isEmpty());
			dbService.factory.remove(admin.id, User.class);
			Assert.assertTrue(dbService.factory.findByString("admin", User.class, "username").getResult().isEmpty());
			int newAdmin = dbService.factory.add(new User("admin", "nuarePAROLA1#@", "Daian", "Dragos", "1234567890123", "home", "city", "state", "email", 1), User.class);

			Assert.assertTrue(dbService.factory.findByString("admin", User.class, "username").getResult().size() == 1);
			dbService.factory.begin(newAdmin, User.class);
			dbService.factory.get(newAdmin, User.class).username = "daian";
			dbService.factory.end(newAdmin, User.class);
			Assert.assertTrue(dbService.factory.findByString("admin", User.class, "username").getResult().isEmpty());
			dbService.factory.remove(2, Book.class);
			dbService.factory.get(3, Book.class).quantity =0;
			new CreateCSV().makeFrom(dbService.factory, "a.txt");
			srv.updateBook(6, null, null, null, 0, 0);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue(false);
		}
	}
}
